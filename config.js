var CONFIG = {
  "maps": {
    "map_myworld": {
      "imageFormat": "png",
      "lastRendered": [
        1542441037,
        0,
        0,
        0
      ],
      "maxZoom": 8,
      "name": "My World",
      "renderView": "isometric",
      "rotations": [
        0
      ],
      "textureSize": 12,
      "tileSetGroup": "myworld_isometric_t1",
      "tileSize": [
        384,
        384
      ],
      "world": "myworld",
      "worldName": "myworld",
      "worldSeaLevel": 64
    }
  },
  "mapsOrder": [
    "map_myworld"
  ],
  "tileSetGroups": {
    "myworld_isometric_t1": {
      "maxZoom": 8,
      "tileOffsets": [
        [
          0,
          0
        ],
        [
          0,
          0
        ],
        [
          0,
          0
        ],
        [
          0,
          0
        ]
      ],
      "tileWidth": 1
    }
  }
};
